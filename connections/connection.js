// importing mongoose module
const mongoose = require('mongoose')

// building connections to the mongo db compass using mongoose connect method
const connectDB = ()=>
        mongoose.connect("mongodb://localhost:27017/usersdata?readPreference=primary&appname=MongoDB%20Compass&ssl=false")
    .then(()=>{
        console.log("Database Connected Successfully please....")
    })

module.exports = connectDB

