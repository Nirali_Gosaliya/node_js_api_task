// import express js module
const express = require("express");

const connectDB =require("./connections/connection");
// express() is used to handle HTTP request
const app = express();
// defining port 
const port = 3000;
 
connectDB();

app.use(express.json())
app.use("/",require("./routes/index"))
app.listen(port, () => {
 console.log(`Your app listening at http://localhost:${port}`);
});
