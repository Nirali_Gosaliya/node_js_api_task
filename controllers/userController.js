
// importing schema from models folder
const Userdata = require('../models/userModel')

// get all users using get method
exports.getUser = (req,resp) => {
    Userdata.find()
    .then((user)=>{
        const filetrdata = user.filter((item,index)=>{
            if(item.deletedat===null){
                return item
            }
        })
        resp.status(200).json({
            message: "User is suceessfully get",
            data: filetrdata
        })
        console.log(filetrdata)
    })
    .catch((err)=>{
        resp.status(200).send({
            message: err.message || "User data is not available",
        })
    })
    
}

// create new user using post method
exports.createUser = async (req,resp) => {
    console.log(req.body)
    if(!req.body){
        resp.status(404).send("Body cannot be empty in post method")
        return
    }
    // create new user
    const user = new Userdata({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        username: req.body.username,
        date:req.body.date
    })

    // check whether user already exist or not
    let checkuser = await Userdata.findOne({username:req.body.username})
        if(!checkuser){
            user.save(user)
    .then((data)=>{
        resp.json(data)
    })
    .catch((err)=>{
        resp.status(500).send({
            message: err.message || 
            "Error has occured"
        })
    })
}
else{
    resp.status(400).send({
        message: "Username must be unique"
    })
}
        
    

    // save user in database
    user.save(user)
    .then((data)=>{
        resp.json(data)
    })
    .catch((err)=>{
        resp.status(500).send({
            message: err.message || 
            "Error has occured while creating a new user"
        })
    })
}

// soft delete using findByIDAndUpdate method
// delete user by id
exports.deleteUser = (req,resp) => {
    const id = req.params.id 
    console.log(id)
    Userdata.findByIdAndUpdate(id,{deletedat:new Date(),status:0})
    .then((result)=>{
        resp.status(200).json({
            message: "User is successfully deleted",
            
        })
    }).catch((err)=>{
        resp.status(500).send({
            message: err.message || "Error has occured while deleting user"
        })
       
    })

}

// update user by id using 
// update user by id
exports.updateuser = (req,resp) => {
   const {id} = req.params
   const {firstname,lastname} = req.body
    console.log(id)
    Userdata.findByIdAndUpdate(id,{firstname,lastname},{new:1})
    .then((result)=>{
        resp.status(200).json({
            message: "User is successfully updated",
            data:result
            
        })
    }).catch((err)=>{
        resp.status(500).send({
            message: err.message || "Error has occured while updating user"
        })
       
    })

}




