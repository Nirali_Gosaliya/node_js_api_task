// importing mongosoe
const mongoose = require('mongoose')

// defining schema of users
const schema = mongoose.Schema({
    firstname: {
        type: String,
        required: [true,"Firstname is required"]
    },
    lastname: {
        type: String,
        required: [true,"Lastname is required"]
    },
    email : {
        type: String,
        required: [true,"email is required"]
    },
    username: {
        type: String,
        require: [true,"username is required"],
        unique: true,
    },
   date: {
       type: String,
       require: [true,"Date is required"],
        match: [/^(0[1-9]|1[0-9]|2[0-9]|3[0-1])[- /.](0[1-9]|1[0-2])[- /.](20|21|22)\d\d$/,"Date format must be dd/mm/yyyy"]
   },
   createdat : {
       type: Date,
       default: new Date()
   },
   updatedat : {
       type: Date,
       default: null
   },
   deletedat: {
    type: Date,
    default: null,
    
   },
   status : {
      type: Number,
      enum: [0,1],
      default: 1
   }
   
})

const Userdata = mongoose.model("User",schema)  //User is collection name without s or es
module.exports = Userdata