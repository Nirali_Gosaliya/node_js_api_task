// importing express module
var express = require('express');
// importing router from express js
var router = express.Router();

// importing userController from controllers file
const userConroller = require('../controllers/userController')
/* GET home page. */

router.get('/getuserdata',userConroller.getUser);
router.post('/createuserdata',userConroller.createUser);
router.patch('/deleteuserdata/:id',userConroller.deleteUser)
router.patch('/updateuserdata/:id',userConroller.updateuser)
module.exports = router;
