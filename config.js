import dotenv from "dotenv";
dotenv.config();
const env = process.env.NODE_ENV;

export const configs = {
  local: {
    port: process.env.PORT,
    environment: "local",
    db_name: process.env.DB_NAME,
    db_user: process.env.DB_USER,
    db_pass: process.env.DB_PASS,
  },
};
